# Haskell-go

## Prerequisites

```
sudo pacman -S stack
stack install alex happy
```

## How to build

```
stack build
```

## How to run

```
stack exec haskell-go-exe <file>.go

# For example
stack exec haskell-go-exe test/execution/recurse1.go
```

## How to run tests
```
stack test
```
