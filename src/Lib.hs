module Lib where

import qualified HappyParser
import Data.Map
import System.Environment
import AST
import Eval
import Debug.Trace

import AlexToken

import Control.Monad.Trans

import Gho

libMain :: IO Eval.State
libMain = fmap head getArgs >>= runFile

runFile :: String -> IO Eval.State
runFile f = do
    cont <- readFile f 
    case (parseGho cont) of 
        (Right a) -> runProgram a
        (Left b) -> error $ "Parse error: " ++ b

    

